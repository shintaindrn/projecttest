﻿namespace TestKasir.Client.Services.PesananService
{
    public interface IPesananService
    {
        List<Pesanan> pesanan { get; set; }

        Task GetPesanan();

        Task<Pesanan> GetPesananId(string id);
    }
}
