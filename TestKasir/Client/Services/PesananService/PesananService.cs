﻿using System.Net.Http.Json;
using System.Runtime.CompilerServices;

namespace TestKasir.Client.Services.PesananService
{
    public class PesananService :IPesananService
    {
        private readonly HttpClient _http;

        public PesananService(HttpClient http)
        {
            _http = http;
        }

        public List<Pesanan> pesanan { get; set; } = new List<Pesanan>();

        public async Task GetPesanan()
        {
            var result = await _http.GetFromJsonAsync<List<Pesanan>>("api/pesanan");

            if (result != null)
                pesanan = result;
        }

        public async Task<Pesanan> GetPesananId(string id)
        {
            var result = await _http.GetFromJsonAsync<Pesanan>($"api/pesanan/{id}");

            if (result != null)
            {
                return result;
            }
            throw new Exception("Pesanan tidak ditemukan");
        }
    }
}
