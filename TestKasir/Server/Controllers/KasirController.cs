﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace TestKasir.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KasirController : ControllerBase
    {
        public static List<Pesanan> pesanan = new List<Pesanan>
        {
            new Pesanan{pesananID = "ABC16122022-001", pesananName="Tasya", total=58000},
            new Pesanan{pesananID = "ABC16122022-002", pesananName="Rizky", total=87000}
        };

        [HttpGet]
        public async Task<ActionResult<List<Pesanan>>> GetPesanan()
        {
            return Ok(pesanan);
        }

        [HttpGet("{Id}")]
        public async Task<ActionResult<List<Pesanan>>> GetPesananId(string id)
        {
            var hero = pesanan.FirstOrDefault(h => h.pesananID == id);
            if (hero == null)
            {
                return NotFound("Maaf, pesanan tidak tersedia");
            }
            return Ok(hero);
        }
    }
}
