﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestKasir.Shared
{
    public class Pesanan
    {
        public string pesananID { get; set; }
        public string pesananName { get; set; }
        public decimal total { get; set; }
    }
}
